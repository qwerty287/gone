package main

import (
	"flag"
	"fmt"
	"strconv"
	"time"

	"github.com/hajimehoshi/oto/v2"
)

var (
	sampleRate      = flag.Int("samplerate", 44100, "sample rate")
	channelNum      = flag.Int("channel", 2, "number of channel")
	bitDepthInBytes = flag.Int("bitdepth", 2, "bit depth in bytes")
)

func main() {
	flag.Parse()
	args := flag.Args()

	if len(args) < 2 || args[0] == "h" || args[0] == "-h" || args[0] == "help" || args[0] == "--help" {
		printHelp()
		return
	}

	context, err := NewContext()
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	duration, err := time.ParseDuration(args[len(args)-1])
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	for i, v := range args {
		if i == len(args)-1 {
			continue
		}

		freq, err := strconv.ParseFloat(v, 64)
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		context.PlayFrequency(freq, duration)
	}
}

func printHelp() {
	fmt.Println(`gone v1.0.0
A simple application that just plays a pure tone. For more information see https://codeberg.org/qwerty287/gone

Usage: gone [options] [frequencies] [duration]
Options:
-samplerate	sample rate
-channel	number of channel
-bitdepth	bit depth in bytes`)
}

type Context struct {
	OtoContext *oto.Context

	sample   int
	channel  int
	bitDepth int
}

func (ctx *Context) PlayFrequencyAndContinue(frequency float64, duration time.Duration) {
	p := ctx.OtoContext.NewPlayer(newWave(frequency, duration, ctx.sample, ctx.channel, ctx.bitDepth))
	p.Play()
}

func (ctx *Context) PlayFrequency(frequency float64, duration time.Duration) {
	ctx.PlayFrequencyAndContinue(frequency, duration)
	time.Sleep(duration)
}

func NewContextWithParameters(sample int, channel int, bitDepth int) (*Context, error) {
	context, ready, err := oto.NewContext(sample, channel, bitDepth)
	if err != nil {
		return nil, err
	}
	<-ready

	return &Context{
		OtoContext: context,
		sample:     sample,
		channel:    channel,
		bitDepth:   bitDepth,
	}, nil
}

func NewContext() (*Context, error) {
	return NewContextWithParameters(*sampleRate, *channelNum, *bitDepthInBytes)
}
