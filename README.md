# gone

A simple application that just plays a pure tone

This application is based on the [example](https://github.com/hajimehoshi/oto/blob/main/example/main.go) of the [`oto` library](https://github.com/hajimehoshi/oto/), which is used by this application.

# Usage

Play a frequency:

`gone [options] [frequencies] [duration]`

The `frequencies` are played for the `duration` and if one frequency finishes, the next starts (the duration must be the last argument). 

### Options
|Option|Usage 
|---|---
|`-samplerate`|sample rate
|`-channel`|number of channel
|`-bitdepth`|bit depth in bytes


View help:

`gone help`

# Build

Requires [Go](https://go.dev/) for compilation, [gofumpt](https://pkg.go.dev/mvdan.cc/gofumpt) for code formatting and [golangci-lint](https://golangci-lint.run/) to run linters.

To build the code
```sh
make
```
or
```sh
make build
```
or if you don't have `make` installed
```sh
go build
```

To reformat the code:
```sh
make fmt
```

To run linters (will automatically format your code with `gofumpt`)
```sh
make lint
```
or if you don't want to format your code (linter may fail)
```sh
make lint-only
```

## Use as library

You can use gone as a library to play frequencies. Therefore, install it with `go get codeberg.org/qwerty287/gone`.

#### Example

```go
package main

import (
	"time"

	"codeberg.org/qwerty287/gone"
)

func main() {
	context, err := gone.NewContext() // returns a gone.Context with default values
	if err != nil {
		return
	}

	// if you need other values as sample rate, channel and bit depth use
	// gone.NewContextWithParameters(sample rate, channel, bit depth)

	frequency := 500.0
	// works best with times under 9 seconds
	duration := 6 * time.Second

	// PlayFrequency waits until sound is finished
	context.PlayFrequency(frequency, duration)

	// PlayFrequencyAndContinue continues after start playing the tone
	context.PlayFrequencyAndContinue(frequency, duration)

	// if you need the oto.Context directly
	context.OtoContext.Suspend()
}
```
