package main

import (
	"io"
	"math"
	"time"
)

type wave struct {
	freq   float64
	length int64
	pos    int64

	remaining []byte
}

func newWave(freq float64, duration time.Duration, sample int, channel int, bitDepth int) *wave {
	l := int64(channel) * int64(bitDepth) * int64(sample) * int64(duration) / int64(time.Second)
	l = l / 4 * 4
	return &wave{
		freq:   freq,
		length: l,
	}
}

func (w *wave) Read(buf []byte) (int, error) {
	if len(w.remaining) > 0 {
		n := copy(buf, w.remaining)
		copy(w.remaining, w.remaining[n:])
		w.remaining = w.remaining[:len(w.remaining)-n]
		return n, nil
	}

	if w.pos == w.length {
		return 0, io.EOF
	}

	eof := false
	if w.pos+int64(len(buf)) > w.length {
		buf = buf[:w.length-w.pos]
		eof = true
	}

	var origBuf []byte
	if len(buf)%4 > 0 {
		origBuf = buf
		buf = make([]byte, len(origBuf)+4-len(origBuf)%4)
	}

	length := float64(*sampleRate) / float64(w.freq)

	num := (*bitDepthInBytes) * (*channelNum)
	p := w.pos / int64(num)
	switch *bitDepthInBytes {
	case 1:
		for i := 0; i < len(buf)/num; i++ {
			const max = 127
			b := int(math.Sin(2*math.Pi*float64(p)/length) * 0.3 * max)
			for ch := 0; ch < *channelNum; ch++ {
				buf[num*i+ch] = byte(b + 128)
			}
			p++
		}
	case 2:
		for i := 0; i < len(buf)/num; i++ {
			const max = 32767
			b := int16(math.Sin(2*math.Pi*float64(p)/length) * 0.3 * max)
			for ch := 0; ch < *channelNum; ch++ {
				buf[num*i+2*ch] = byte(b)
				buf[num*i+1+2*ch] = byte(b >> 8)
			}
			p++
		}
	}

	w.pos += int64(len(buf))

	n := len(buf)
	if origBuf != nil {
		n = copy(origBuf, buf)
		w.remaining = buf[n:]
	}

	if eof {
		return n, io.EOF
	}
	return n, nil
}
