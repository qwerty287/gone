.PHONY: all
all: build

.PHONY: build
build: 
	go build

.PHONY: fmt
fmt:
	gofumpt -w .

.PHONY: lint-only
lint-only:
	golangci-lint run

.PHONY: lint
lint: fmt lint-only
